#!/usr/bin/env python3
#PBS -V # export all environment variables to the batch job.
#PBS -q mrchq # submit to the serial queue
#PBS -l walltime=1:00:00 # Maximum wall time for the job.
#PBS -A Research_Project-MRC148213
#PBS -l procs=32 # specify number of processors.
#PBS -m e -M sl693@exeter.ac.uk # email me at job completion

##############################################################################################################
# 16th April 2019: Tabulate ccs output reports into merged final output
# 24th July 2019: allowed user argument in for working directory
# Note python script only adopted to process output files from CCS (v3.4.1)
#############################################################################################################

import os 
import sys
import pandas as pd
from glob import glob
from pathlib import Path

# input arguments from argument command line
ccs_input = sys.argv[1] # 1st argument = dir containing ccs_input
output_dir = sys.argv[2] # 2nd argument = dir for output.csv
output_name = sys.argv[3] # 3rd argument = output name for csv
print "Processing : %s" % output_name 
print "Processing CCS_output from dir: %s" % ccs_input   

# setting working directory
ccs_input = str(ccs_input)
os.chdir(ccs_input)
# check in working directory
# print "Current working dir : %s" % os.getcwd()

# list all files with ccs.report 
filenames = glob('*ccs_report.txt')
print(filenames)

# loop txt.files into one list 
dataframes = [pd.read_csv(f,sep='\t') for f in filenames]
dataframes

# Generate sample names based on the names of files, extracting the first charachter of the strings delimited by ".""
samples = []
for f in filenames : 
    f = f.split('_')[0]
    print(f)
    samples.append(str(f))
    print(samples)

# To split column into several columns as separated with commas, and save to df1 column
# To label the columns as the sample names (list saved from above)
# Set the index of the dataframe as the descriptions
mod = []
count = 0 
for df in dataframes :
    df1 = df['ZMW Yield'].str.split(',', expand=True)
    df1.columns = ['ZMW Yield', samples[count], samples [count]]
    df1 = df1.set_index('ZMW Yield')
    mod.append(df1)
    count += 1

# Concentenate dataframes
final = pd.concat(mod, axis = 1)

# saving final.df based on arguments 
file_end = '_CCS_output.csv'
complete_file = output_dir + output_name + file_end
final.to_csv(complete_file)
