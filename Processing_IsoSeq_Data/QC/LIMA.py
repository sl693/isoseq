##############################################################################################################
# Date: 16th April 2019
# Tabulate ccs output reports into merged final output
#############################################################################################################
import os 
import sys
import pandas as pd
from glob import glob
from pathlib import Path

# input arguments from argument command line
lima_input = sys.argv[1] # 1st argument = dir containing lima_input
output_dir = sys.argv[2] # 2nd argument = dir for output.csv
output_name = sys.argv[3] # 3rd argument = output name for csv
print "Processing : %s" % output_name 
print "Processing LIMA_output from dir: %s" % lima_input  

# setting working directory
lima_input = str(lima_input)
os.chdir(lima_input)
# check in working directory
# print "Current working dir : %s" % os.getcwd()

# list all files with ccs.report 
filenames = glob('*lima.summary')
filenames

# loop txt.files into one list 
dataframes = [pd.read_csv(f,sep='\t',header = None) for f in filenames]
dataframes

# Generate sample names based on the names of files, extracting the first charachter of the strings delimited by ".""
samples = []
for f in filenames : 
    f = f.split('.')[0]
    samples.append(str(f))
    print(samples)

# To split column into several columns as separated with commas, and save to df1 column
# To label the columns as the sample names (list saved from above)
# Set the index of the dataframe as the descriptions
mod = []
count = 0 
for df in dataframes :
    df.columns = ['Description']
    df1 = df['Description'].str.split(':', expand=True)
    print(len(df1))
    df1.columns = ['Description', samples[count]]
    df1 = df1.set_index('Description')
    mod.append(df1)
    count += 1

# Concentenate dataframes
final = pd.concat(mod, axis = 1)

# saving final.df based on arguments 
file_end = '_LIMA_summary.csv'
complete_file = output_dir + output_name + file_end
final.to_csv(complete_file)
