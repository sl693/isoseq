#!/bin/sh
#PBS -V # export all environment variables to the batch job.
#PBS -q mrcq # submit to the serial queue
#PBS -l walltime=144:00:00 # Maximum wall time for the job.
#PBS -A Research_Project-MRC148213
#PBS -l procs=32 # specify number of processors.
#PBS -m e -M sl693@exeter.ac.uk # email me at job completion

# Aim: Generate CCS read lengths across multiple Iso-Seq datasets 
# 27/08/2019: Generate QC Plots for mouse isoseq WT8 data
# 25/11/2019: Tie up all Pipeline for Mouse QC CCS with distribution and CCS
# 26/11/2019: Include Pipeline to Human and by writing as functions 

#************************************** Global Variables
module load Miniconda2/4.3.21
module load R

# Scripts Directory 
FUNCTIONS_LENGTH=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Scripts/general/QC_Figures/IsoSeq
FUNCTIONS_PLOTS=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Scripts/general/QC_Figures/Rscripts

# Output Directory 
OUTPUT=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Isoseq_Paper/QC

# Input Directory 
MOUSE_IND_ISOSEQ3=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/WholeTranscriptome/Individual/Isoseq3.1.2/Isoseq3
MOUSE_MERGED_ISOSEQ3=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2
Fetal_CCS_Input=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/CCS
Adult_CCS_Input=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Adult/CCS
Adult_Aaron_Input=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Adult
Fetal_Aaron_Input=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Fetal
#cd $OUTPUT; mkdir CCS_output POLISH_output TOFU_output SQANTI_output ISOSEQ3_output
#cd $OUTPUT/CCS_output; mkdir mouse fetal adult 
#cd $Fetal_CCS_Input; mkdir Cortex Hippocampus Striatum

################################################################################################
#************************************** Prepare Human Files (copy over from Aaron's folder)
Transfer_Human_Files(){
    # Fetal CCS files 
    if [ -f "$Fetal_CCS_Input/Cortex/11921FL_ccs_report.txt" ]; then
        echo "Fetal ccs files exist, no need to copy"
        else
        cp $Fetal_Aaron_Input/*FL.bam* $Fetal_CCS_Input/Cortex
        cp $Fetal_Aaron_Input/*FL_ccs_report.txt* $Fetal_CCS_Input/Cortex

        cp $Fetal_Aaron_Input/*FL2.bam* $Fetal_CCS_Input/Cortex
        cp $Fetal_Aaron_Input/*FL2_ccs_report.txt* $Fetal_CCS_Input/Cortex

        cp $Fetal_Aaron_Input/*STR.bam* $Fetal_CCS_Input/Striatum
        cp $Fetal_Aaron_Input/*STR_ccs_report.txt* $Fetal_CCS_Input/Striatum

        cp $Fetal_Aaron_Input/*HIP.bam* $Fetal_CCS_Input/Hippocampus
        cp $Fetal_Aaron_Input/*HIP_ccs_report.txt* $Fetal_CCS_Input/Hippocampus
    fi

    # Fetal Polished files 
    if [ -f "$Fetal_POLISH_Input/fetalFL.polished.hq.fastq" ]; then
        echo "Fetal polished.fastq.hq files exist, no need to copy"
        else
        cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Fetal/CTX/fetalFL.polished.hq.fastq $Fetal_POLISH_Input
        cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Fetal/HIP/fetalHIP.polished.hq.fastq $Fetal_POLISH_Input
        cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Fetal/STR/fetalSTR.polished.hq.fastq $Fetal_POLISH_Input 
    fi

    # Adult CCS files 
    if [ -f "$Adult_CCS_Input/1193A_Twin2.ccs.bam" ]; then
        echo "Adult ccs files exist, no need to copy"
        else
        # copy over samples for Aaron's folder specifically CCS (labelled as 1.bam, 2.bam)
        for i in {1..5}; do 
        cp $Adult_Aaron_Input/$i.bam* $Adult_CCS_Input
        done 
        # name files appropriately
        ADULT_SAMPLES_NAMES=(A194_95 A002_94 A002_94b 1191A_Twin1 1193A_Twin2)
        count=0
        for i in ${ADULT_SAMPLES_NAMES[@]}; do 
        echo $i
        count=$((count+1))
        mv $count.bam $i.ccs.bam
        mv $count.bam.pbi $i.ccs.bam.pbi
        mv $count_ccs_report.txt $i_ccs_report.txt
        done 
    fi
}


################################################################################################
#************************************** Tabulate CCS and LIMA Output Statistics from Isoseq3.1.2
# parse_stats_per_sample <input_ccs.bam_dir> <output_ccs_dir> <input_lima_dir> <output_lima_dir> </sample_name> <output_prefix_name>
parse_stats_per_sample(){
    # Load env for modules
    source activate py2.7.12

    echo "Processing ccs output for $6"
    # collate CCS_output/sample 
    # python ccs.py <input_ccs_directory> <output_directory> </sample_name>
    # !!important to have "/" in front of the sample name, for .csv file to be saved correctly!!
    python $FUNCTIONS_LENGTH/CCS.py $1 $2 $5 > $6.ccs.stats.log

    echo "Processing lima output for $6"
    # collate Lima_output/sample 
    # python lima.py <input_lima_directory> <output_directory> </sample_name>
    # !!important to have "/" in front of the sample name, for .csv file to be saved correctly!!
    python $FUNCTIONS_LENGTH/LIMA.py $3 $4 $5 > $6.lima.stats.log

    source deactivate
}

################################################################################################
#************************************** FUNCTIONS
# ccs_bam2fasta: convert ccs_bam to fasta 
# ccs_bam2fasta <sample_name> <input_ccs.bam_dir> <output_dir>
# output file = <sample_name>.ccs.fasta
ccs_bam2fasta(){
    module load Miniconda2
    source activate bcbiogff
    bam2fastq --version

    ccs_file=$(find $2 -name "*.bam" | grep $1)
    echo "Converting CCS of sample $1 from bam to fasta"
    echo $ccs_file 

    cd $3
    bam2fasta -u -o $1.ccs $ccs_file 2>$1.bam2fasta.log
    source deactivate    
}

global_length(){
    module load Miniconda2
    source activate sqanti2
    CUPCAKE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake
    export PYTHONPATH=$PYTHONPATH:$CUPCAKE/sequence
    export PATH=$PATH:$CUPCAKE/sequence
}

# ccs_length: Summarize sequence lengths in ccs fasta files 
# ccs_length <sample_name> <input_ccs.fasta_dir> <output_dir>
# output file = <sample_name>.ccs.fasta.seqlengths.txt
ccs_length(){

    global_length #prepare conda env

    cd $3
    echo "Extracting length of Sample $1 CCS"
    get_seq_stats.py $2/$1.ccs.fasta 2>$1.ccs.get_seq.log

    source deactivate
}

################################################################################################
#************************************** Calculate Length of Fastq input 

## Individual 
# parse_ccs_per_sample <input_ccs.bam_dir> <output_ccs_dir> 
# Aim: To convert ccs.bam to ccs.fasta, and determine the length distribution
# Prerequisite: Always have SAMPLE_NAMES before running function

parse_ccs_per_sample(){
    
    for i in ${SAMPLES_NAMES[@]}; do
        # ccs_bam2fasta <sample_name> <input_ccs.bam_dir> <output_dir>
        ccs_bam2fasta $i $1 $2
        ccs_length $i $2 $2
    done
}

## Merged 
# parse_ccs_merged_samples <sample_ccs_dir/*ccs.fasta> <sample_output_prefix> <output_dir>
# Aim: To merge all the ccs.fasta files to see the merged distribution per sample 
# Prerequisite: run parse_ccs_per_sample as require ccs.fasta as input 
parse_ccs_merged_samples(){

    source activate sqanti2    
    cd $3

    echo "Merging the following fasta files to $2_merged.fasta"
    ls $1/*fasta 

    cat $1/*fasta > $2_merged.fasta
    get_seq_stats.py $2_merged.fasta 2> $2_merged.ccs.get_seq.log
    mv $2_merged.fasta.seqlengths.txt $2_merged.fastq.seqlengths.txt  # for consistent file naming as fastq for downstream processing in rscript for plots

    source deactivate
}

################################################################################################
#************************************** Run Functions

Transfer_Human_Files

# Mouse 
SAMPLES_NAMES=(Q21 O23 S23 K17 C21 B21 M21 K23)
parse_stats_per_sample $MOUSE_MERGED_ISOSEQ3/CCS $OUTPUT/CCS_output/mouse $MOUSE_MERGED_ISOSEQ3/Isoseq3_WKD $OUTPUT/ISOSEQ3_output /WT8Mouse_Merge WT8Mouse_Merge
parse_stats_per_sample $MOUSE_MERGED_ISOSEQ3/CCS $OUTPUT/CCS_output/mouse $MOUSE_IND_ISOSEQ3/LIMA $OUTPUT/ISOSEQ3_output /WT8Mouse_Individual WT8Mouse_Individual
parse_ccs_per_sample $MOUSE_MERGED_ISOSEQ3/CCS $OUTPUT/CCS_output/mouse 
parse_ccs_merged_samples $OUTPUT/CCS_output/mouse mouse $OUTPUT/CCS_output/mouse

# Fetal
SAMPLES_NAMES=(11921FL 12971FL 12972FL 12971FL2 12972FL2 12971HIP 12972HIP 12971STR 12972STR)
parse_stats_per_sample $Fetal_CCS_Input $OUTPUT/CCS_output/fetal $Fetal_Aaron_Input $OUTPUT/ISOSEQ3_output /Fetal_Merged
parse_ccs_per_sample $Fetal_CCS_Input $OUTPUT/CCS_output/fetal 
parse_ccs_merged_samples $OUTPUT/CCS_output/fetal fetal $OUTPUT/CCS_output/fetal

# Adult 
SAMPLES_NAMES=(A194_95 A002_94 A002_94b 1191A_Twin1 1193A_Twin2)
parse_stats_per_sample $Adult_CCS_Input $OUTPUT/CCS_output/adult $Adult_Aaron_Input $OUTPUT/ISOSEQ3_output /Adult_Merged
parse_ccs_per_sample $Adult_CCS_Input $OUTPUT/CCS_output/adult 
parse_ccs_merged_samples $OUTPUT/CCS_output/adult adult $OUTPUT/CCS_output/adult