#!/bin/sh
#PBS -V # export all environment variables to the batch job.
#PBS -q mrchq # submit to queue
#PBS -l walltime=144:00:00 # Maximum wall time for the job in hours:minutes:seconds
#PBS -A Research_Project-MRC148213 # research project to submit under.
#PBS -l procs=32 # specify number of processors.
#PBS -m e -M sl693@exeter.ac.uk # email me at job completion

module load Miniconda2 
source activate sqanti2_py3

#************************************* DEFINE GLOBAL VARIABLES
FETAL_TOFU=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Fetal
MOUSE_TOFU=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/TOFU
ADULT_TOFU=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Adult
HUMAN_TOFU=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/FetalAdult

AARON_SQANTI2=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/SQ2_v7
MOUSE_SQANTI2=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/SQANTI2_v7/WT8

ADULT_RAREFACTION=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Adult/RAREFACTION
FETAL_RAREFACTION=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/RAREFACTION
MOUSE_RAREFACTION=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/RAREFACTION
HUMAN_RAREFACTION=/gpfs/mrc0/projects/Research_Project-MRC148213/Human/RAREFACTION

CUPCAKE_ANNOTATION=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/annotation

# make_file_for_rarefaction <sample_name_prefix> <input_tofu_directory> <input_sqanti_directory> <working_directory>
make_file_for_rarefaction(){
	cd $3
	echo "Working with $1" 
	# make_file_for_subsampling_from_collapsed.py <sample_name_prefix>.input.file <sample_name_prefix>.output.file <sample_name_prefix>.classification.txt
	python $CUPCAKE_ANNOTATION/make_file_for_subsampling_from_collapsed.py -i $2/$1.collapsed.filtered -o $1.subsampling -m2 $4/$1.collapsed.filtered.rep_classification.txt
	
	python $CUPCAKE_ANNOTATION/subsample_with_category.py --by refisoform --min_fl_count 2 --step 1000 $1.subsampling.all.txt > $1.rarefaction.by_refisoform.min_fl_2.by_category.txt
}

make_file_for_rarefaction WT8IsoSeq $MOUSE_TOFU $MOUSE_RAREFACTION $MOUSE_SQANTI2
make_file_for_rarefaction adult.sample $ADULT_TOFU $ADULT_RAREFACTION $AARON_SQANTI2
make_file_for_rarefaction combinedFL.sample $HUMAN_TOFU $HUMAN_RAREFACTION $AARON_SQANTI2
make_file_for_rarefaction fetalFL.sample $FETAL_TOFU/CTX $FETAL_RAREFACTION $AARON_SQANTI2
make_file_for_rarefaction fetalHIP.sample $FETAL_TOFU/HIP $FETAL_RAREFACTION $AARON_SQANTI2
make_file_for_rarefaction fetalSTR.sample $FETAL_TOFU/STR $FETAL_RAREFACTION $AARON_SQANTI2

