################################################################################################
## Define functions for Human_Post_Isoseq3.1.2.sh
# PreRequisite: Isoseq3.1.2 on Human Fetal and Adult samples, Map and Tofu collapse 
# 1) run_STAR_human <input_rnaseq_sample_name> <input_rnaseq_dir> <output_dir> <STAR_reference_input_directory>
# 2) post_STAR_process <output_sample_name> <input/output_dir>
# 3) human_merge_fastq <Fetal_RNASEQ_input_dir> <Fetal_Kallisto_output_dir> <Adult_RNASEQ_input_dir>
# 4) run_post_isoseq3 <sample_prefix_input/output_name> <isoseq3_input_directory> <mapping_output_directory> <tofu_output_directory>
# 5) run_sqanti2_QC_human <input_tofu_file> <input_tofu_prefix> <coverage/genome=own_rnaseq/introplis/chess> <input_kallisto_file> <output_dir>
# 6) run_sqanti2_Filter <input/output_sample_prefix> <SQANTI2_input/output_dir>
################################################################################################
# 27/11/2019: Created script using Kalisto_RSEM.sh and Human_STAR_Featurecounts.sh
# 23/03/2020: run_sqanti2_QC_human to allow also choice to use chess.gtf for alignment
    # copied run_post_isoseq3 and run_sqanti2_Filter from WT8_Isoseq3.1.2_Functions.sh

module load Miniconda2/4.3.21
################################################################################################
#************************************* RNAseq [Function 1, 2] 
# run_STAR_human <input_rnaseq_sample_name> <input_rnaseq_dir> <output_dir> <STAR_reference_input_directory>
# Aim: Individually align RNASeq filtered fastq files to STAR-indexed genome using STAR, and subsequently input SJ.out.bed file into SQANTI2
# Note: run_STAR_human and run_star (WT8_Isoseq3.1.2.sh) has the exact same command for STAR, but just different input of F_File and R_File
run_STAR_human(){
    
    source activate sqanti2
    cd $2
    echo "Finding reads for Sample $1 for STAR" 
    # find reverse and forward file, trimming "./" therefore only printing file name
    F=$(find . | grep "fq" | grep $1 | grep "r1" | sed 's|^./||' ) 
    R=$(find . | grep "fq" | grep $1 | grep "r2" | sed 's|^./||' )
    # save path directory of files as variable for later mapping
    F_File=$(realpath $F)
    R_File=$(realpath $R)
    echo "Processing Forward Reads: $F_File"
    echo "Processing Reverse Reads: $R_File"

    cd $3
    STAR --runMode alignReads --runThreadN 64 --genomeDir $4/hg38 \
    --readFilesIn $F_File $R_File \
    --outSAMtype BAM SortedByCoordinate \
    --chimSegmentMin 25 \
    --chimJunctionOverhangMin 20 \
    --chimOutType WithinBAM \
    --chimFilter banGenomicN \
    --chimOutJunctionFormat 1 \
    --twopassMode Basic \
    --twopass1readsN -1 #use all reads

    echo "Processing Aligned sorted by Coordinated bam for $1" 
    mv Aligned.sortedByCoord.out.bam $1.sorted.bam
    picard MarkDuplicates INPUT=$1.sorted.bam OUTPUT=$1.sorted.nodup.bam METRICS_FILE=$1.dup.txt VALIDATION_STRINGENCY=LENIENT REMOVE_DUPLICATES=true \
    2> $1.PicardDup.log

    ls -alth *
}

# post_STAR_process <output_sample_name> <input/output_dir>
post_STAR_process(){
    cd $2
    ls -alth *Aligned.sortedByCoord.out.bam
    echo "Processing Aligned sorted by Coordinated bam for $1" 
    mv Aligned.sortedByCoord.out.bam $1.sorted.bam
    picard MarkDuplicates INPUT=$1.sorted.bam OUTPUT=$1.sorted.nodup.bam METRICS_FILE=$1.dup.txt VALIDATION_STRINGENCY=LENIENT REMOVE_DUPLICATES=true \
    2> $1.PicardDup.log

    # rename output files
    mv Log.final.out $1.Log.final.out 
    mv Log.out $1.Log.out
    mv Log.progress.out $1.Log.progress.out
    mv SJ.out.tab $1.SJ.out.bed
}

# human_merge_fastq <Fetal_RNASEQ_input_dir> <Fetal_Kallisto_output_dir> <Adult_RNASEQ_input_dir> <Adult_Kallisto_output_dir>
# Aim: Merge forward and reverse of all RNASeq filtered fastq, for further downstream alignment to IsoSeq Tofu output using Kallisto
# PreRequisite: Ensure specified raw fastq files defined from function are in RNASEq_input_dir 
# Output: Fetal_R1.fq, Fetal_R2.fq, Adult_R1.fq, Adult_R2.fq
human_merge_fastq(){
    ## Fetal 
    cd $1
    echo "Processing Fetal R1 files"
    ls *r1.fq
    cat 3005_11921_trimmed_r1.fq  3005_12971FL_trimmed_r1.fq  3005_12972FL_trimmed_r1.fq  > $2/Fetal_R1.fq
    
    echo "Processing Fetal R2 files"
    ls *r2.fq
    cat 3005_11921_trimmed_r2.fq  3005_12971FL_trimmed_r2.fq  3005_12972FL_trimmed_r2.fq  > $2/Fetal_R2.fq

    ## Adult 
    cd $3
    echo "Processing Adult R1 files"
    ls *R1*
    cat 24_ATTCCT_L001_R1_001.fastq  25_ATCACG_L008_R1_001.fastq > $4/Adult_R1.fq
    
    echo "Processing Adult R2 files"
    ls *R2*
    cat 24_ATTCCT_L001_R2_001.fastq  25_ATCACG_L008_R2_001.fastq > $4/Adult_R2.fq
}

################################################################################################
#************************************* Post_Isoseq3 (Minimap2, Cupcake) [Function 3] 
# run_post_isoseq3 <sample_prefix_input/output_name> <isoseq3_input_directory> <mapping_output_directory> <tofu_output_directory>
run_post_isoseq3(){
    
    CUPCAKE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake
    SQANTI2_dir=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/SQANTI2
    REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019
    ANNOTATION=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/annotation
    SEQUENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/sequence

    source activate sqanti2_py3

    # convert between fasta and fastq for downstream process
    echo "fasta2fastq conversion"
    python $SEQUENCE/fa2fq.py $2/$1.polished.hq.fasta 
    
    samtools --version # echo version
    echo "Minimap2 version:" $(minimap2 --version) # echo version

    echo "Processing Sample $1 for Minimap2 and sort"
    cd $3 #cd to $MAP directory for output
    minimap2 -t 30 -ax splice -uf --secondary=no -C5 -O6,24 -B4 $REFERENCE/hg38.fa $2/$1.polished.hq.fastq > $1.sam 2> $1.map.log
    samtools sort -O SAM $1.sam > $1.sorted.sam
    
    head $CUPCAKE/README.md
    echo "Processing Sample $1 for ToFU collapse"
    cd $4 #cd to $TOFU directory for output
    if python $CUPCAKE/cupcake/tofu/collapse_isoforms_by_sam.py --input $2/$1.polished.hq.fastq --fq -s $3/$1.sorted.sam --dun-merge-5-shorter -o $1 \
    2> $1.collapse.log; then 
        echo "Processing Sample $1 for collapse successful"
    else 
        echo "Processing Sample $1 for collapse failed"
    fi
    
    if python $CUPCAKE/cupcake/tofu/get_abundance_post_collapse.py $1.collapsed $2/$1.cluster_report.csv 2> $1.abundance.log; then 
        echo "Processing Sample $1 for getting abundance successful"
    else 
        echo "Processing Sample $1 for getting abundance failed"
    fi

    if python $CUPCAKE/cupcake/tofu/filter_away_subset.py $1.collapsed 2> $1.filter.log; then 
        echo "Processing Sample $1 for filtering successful"
    else 
        echo "Processing Sample $1 for filtering failed" 
    fi

    seqtk seq -a $1.collapsed.filtered.rep.fq > $1.collapsed.filtered.rep.fa
    head *log

    source deactivate
}


################################################################################################
#************************************* SQANTI2 [Function] 
# run_sqanti2_QC_human <input_tofu_file> <input_tofu_prefix> <coverage/genome=own_rnaseq/introplis/chess> <input_kallisto_file> <output_dir> 
# PreRequisite: 
    # if running "own_rnaseq", define $MAPPED directory containing SJ.out.bed files in working script
    # Define $REFERENCE directory containing gencode.v31.annotation.gtf
# Note: Function specific to human by reference genome input, and also have to state whether using "own_rnaseq" or "introplis" data as input; "chess" for usig intropolis dataset but chess.gtf 
run_sqanti2_QC_human(){

    source activate sqanti2_py3
    echo "Processing Sample $2 for SQANTI2 QC"
    cd $5

    SQANTI2_DIR=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/SQANTI2
    CUPCAKE_SEQUENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/sequence

    export PYTHONPATH=$PYTHONPATH:$CUPCAKE_SEQUENCE
    python $SQANTI2_DIR/sqanti_qc2.py --version    

    if [ $3 == "own_rnaseq" ] 
    then 
        echo "Processing with own RNASeq dataset and genocode.gtf for genome annotation "
        echo "Collapsing with the following bed files"
        echo $MAPPED/*SJ.out.bed

        if python $SQANTI2_DIR/sqanti_qc2.py -t 30 \
        $1/$2.collapsed.filtered.rep.fa \
        $REFERENCE/gencode.v31.annotation.gtf \
        $REFERENCE/hg38.fa \
        --cage_peak $REFERENCE/hg38.cage_peak_phase1and2combined_coord.bed \
        --coverage "$MAPPED/*SJ.out.bed" \
        --expression $4 \
        --polyA_motif_list $SQANTI2_DIR/../human.polyA.list.txt \
        --fl_count $1/$2.collapsed.abundance.txt \
        &> $2.sqanti.qc.log; then
            echo "Processing Sample $2 for SQANTI2 successful" # convert SQANTI2 output sam file to output bam file for Alignqc input 
            samtools view -S -b $2.collapsed.filtered.rep.renamed_corrected.sam > $2.collapsed.filtered.rep.renamed_corrected.bam 
        else 
            echo "Processing Sample $2 for SQANTI2 failed" 
        fi       
    elif [ $3 == "intropolis" ]
    then
        echo "Processing with own intropolis and genocode.gtf for genome annotation "
        echo "Collapsing with the following coverage expression file"
        coverage=$(echo /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/SQANTI2addons/intropolis.v1.hg19_with_liftover_to_hg38.tsv.min_count_10.modified)
        echo $coverage

        if python $SQANTI2_DIR/sqanti_qc2.py -t 30 \
        $1/$2.collapsed.filtered.rep.fa \
        $REFERENCE/gencode.v31.annotation.gtf \
        $REFERENCE/hg38.fa \
        --cage_peak $REFERENCE/hg38.cage_peak_phase1and2combined_coord.bed \
        --coverage $coverage \
        --polyA_motif_list $SQANTI2_DIR/../human.polyA.list.txt \
        --expression $4 \
        --fl_count $1/$2.collapsed.abundance.txt \
        &> $2.sqanti.qc.log; 
        then
            echo "Processing Sample $2 for SQANTI2 successful" # convert SQANTI2 output sam file to output bam file for Alignqc input 
            samtools view -S -b $2.collapsed.filtered.rep_corrected.sam > $2.collapsed.filtered.rep_corrected.bam 
        else 
            echo "Processing Sample $2 for SQANTI2 failed" 
        fi   
    elif [ $3 == "chess" ]
    then
        echo "Processing with intropolis and chess.gtf for genome annotation"
        echo "Collapsing with the following coverage expression file"
        coverage=$(echo /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/SQANTI2addons/intropolis.v1.hg19_with_liftover_to_hg38.tsv.min_count_10.modified)
        echo $coverage

        if python $SQANTI2_DIR/sqanti_qc2.py -t 30 \
        $1/$2.collapsed.filtered.rep.fa \
        $REFERENCE/chess2.2.gtf \
        $REFERENCE/hg38.fa \
        --cage_peak $REFERENCE/hg38.cage_peak_phase1and2combined_coord.bed \
        --coverage $coverage \
        --polyA_motif_list $SQANTI2_DIR/../human.polyA.list.txt \
        --expression $4 \
        --fl_count $1/$2.collapsed.abundance.txt \
        &> $2.sqanti.qc.log; 
        then
            echo "Processing Sample $2 for SQANTI2 successful" # convert SQANTI2 output sam file to output bam file for Alignqc input 
            samtools view -S -b $2.collapsed.filtered.rep_corrected.sam > $2.collapsed.filtered.rep_corrected.bam 
        else 
            echo "Processing Sample $2 for SQANTI2 failed" 
        fi   
    else
        echo "ERROR: Require 2nd argument to be either: own_rnaseq OR intropolis OR chess"
        return  
    fi  
}

# run_sqanti2_Filter <input/output_sample_prefix> <SQANTI2_input/output_dir>
run_sqanti2_Filter(){

    source activate sqanti2_py3

    SQANTI2_DIR=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/SQANTI2
    CUPCAKE_SEQUENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/sequence
  
    echo "Processing Sample $2 for SQANTI2 filter"
    cd $2

    export PYTHONPATH=$PYTHONPATH:$CUPCAKE_SEQUENCE
    python $SQANTI2_DIR/sqanti_qc2.py -v

    if python $SQANTI2_DIR/sqanti_filter2.py \
        $2/$1.collapsed.filtered.rep_classification.txt \
        $2/$1.collapsed.filtered.rep_corrected.fasta  \
        $2/$1.collapsed.filtered.rep_corrected.gtf \
        -a 0.6 -c 3 &> $1.sqanti.filter.log; then
        echo "Processing Sample $1 for SQANTI2 filter successful"
    else
        echo "Processing Sample $1 for SQANTI2 filter failed"
    fi

    source deactivate    
}
