#!/bin/sh
#PBS -V # export all environment variables to the batch job.
#PBS -q mrchq # submit to queue
#PBS -l walltime=144:00:00 # Maximum wall time for the job in hours:minutes:seconds
#PBS -A Research_Project-MRC148213 # research project to submit under.
#PBS -l procs=32 # specify number of processors.
#PBS -m e -M sl693@exeter.ac.uk # email me at job completion

# From Human_STAR_Featurecounts.sh
    # 28/08/2019: align human fetal RNA Seq samples, featurecount and input SQANTI2
    # 20/11/2019: run_sqanti2_QC_merge with user defined variable for coverage of whether own rnaseq data input or intropolis 
    # 22/11/2019: run_sqanti2_QC_merge applicable to any datasets
# 27/11/2019: Rewrote script based on Kalisto_RSEM.sh and Human_STAR_Featurecounts on human samples 
# 23/03/2020: run_post_isoseq3 on Adult samples (using Aaron's isoseq3 polished.fa)
            # run_sqanti2_QC_human on Fetal and Adult with input of chess.gtf (SQANTI2 v7.4)
            # run_sqanti2_Filter on Fetal and Adult
# 25/08/2020: run_sqanti2_QC_human on human with input of chess.gtf and run_sqanti2_Filter (SQANTI2 v7.4)
# 25/08/2020: run_SQANTI2_QC human with fetal RNASeq data as input 

#************************************* DEFINE GLOBAL VARIABLES
#28/08/2019: Transfered files from Aaron's folder 
#$TOFU=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/TOFU
#$REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/fetalRNASeq/ftp1.sequencing.exeter.ac.uk/H0231/11_trimmed/*fq $RAW
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/reference/hg38/* $REFERENCE/hg38
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/reference/hg38.fa* $REFERENCE
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Fetal/CTX/fetalFL.sample.collapsed.filtered.rep.fa $TOFU
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Fetal/CTX/fetalFL.sample.collapsed.abundance.txt $TOFU
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/reference/gencode.v31.annotation.gtf $REFERENCE
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/SQANTI2addons/hg38.cage_peak_phase1and2combined_coord.bed $REFERENCE

#20/11/2019: Transfered files from Aaron's folder 
#ADULT_RNASEQ_dir=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/fetalRNASeq/Adult/RNA_seq_files
#cp $ADULT_RNASEQ_dir/Sample_24/raw_illumina_reads/24_ATTCCT_L001_R1_001.fastq $ADULT_RNASEQ
#cp $ADULT_RNASEQ_dir/Sample_25/raw_illumina_reads/25_ATCACG_L008_R1_001.fastq $ADULT_RNASEQ
#cp $ADULT_RNASEQ_dir/Sample_24/raw_illumina_reads/24_ATTCCT_L001_R2_001.fastq $ADULT_RNASEQ 
#cp $ADULT_RNASEQ_dir/Sample_25/raw_illumina_reads/25_ATCACG_L008_R2_001.fastq $ADULT_RNASEQ
#cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Adult/adult.sample.collapsed.filtered.rep.fa $ADULT_TOFU

# 23/03/2020: copy over isoseq3.1.2 adult polished.hq
#cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Adult/adult.polished.cluster_report.csv $ADULT_TOFU 
#mv $ADULT_TOFU/adult.polished.cluster_report.csv $ADULT_TOFU/adult.cluster_report.csv
#cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/Adult/adult.polished.hq.fasta $ADULT_TOFU

# 14/07/2020 
cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/FetalAdult/combinedFL.sample.collapsed.filtered.rep.fa /gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Human/TOFU/
cp /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/FetalAdult/combinedFL.sample.collapsed.abundance.txt /gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Human/TOFU/

REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019
FUNCTIONS=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Scripts/human_mouse_isoseq/Isoseq_Post/Merge

#***************** FETAL
FETAL_TOFU=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/TOFU
FETAL_KALLISTO_OUTPUT=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/KALLISTO
FETAL_RNASEQ=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/raw
FETAL_SQANTI2_output_dir_rnaseq=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/SQANTI2/own_rnaseq
FETAL_SQANTI2_output_dir_intropolis=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/SQANTI2/intropolis
FETAL_SQANTI2_output_dir_chess=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/SQANTI2/chess
MAPPED=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Fetal/mapped

#***************** ADULT
ADULT_TOFU=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Adult/TOFU
ADULT_KALLISTO_OUTPUT=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Adult/KALLISTO
ADULT_RNASEQ=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Adult/raw
ADULT_SQANTI2_output_dir_intropolis=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Adult/SQANTI2/intropolis
ADULT_SQANTI2_output_dir_chess=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Adult/SQANTI2/chess
ADULT_MAPPING=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Adult/MAPPING 

#***************** HUMAN (FETAL + ADULT combined)
HUMAN_TOFU=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Human/TOFU
HUMAN_KALLISTO_OUTPUT=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Human/KALLISTO
HUMAN_SQANTI2_output_dir_chess=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Human/SQANTI2/chess
ADULT_MAPPING=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Adult/MAPPING 
HUMAN_SQANTI2_output_dir_fetal=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Human/SQANTI2/own_rnaseq

module load Miniconda2 
source activate sqanti2_py3

STAR --version
featureCounts -v
samtools --version

SQANTI2_DIR=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/SQANTI2
CUPCAKE_SEQUENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/sequence


################################################################################################
#************************************* RNAseq
source $FUNCTIONS/Human/Human_Post_Isoseq3.1.2_Functions.sh

# human_merge_fastq <Fetal_RNASEQ_input_dir> <Fetal_Kallisto_output_dir> <Adult_RNASEQ_input_dir> <Adult_Kallisto_output_dir>
human_merge_fastq $FETAL_RNASEQ $FETAL_KALLISTO_OUTPUT $ADULT_RNASEQ $ADULT_KALLISTO_OUTPUT
cd $HUMAN_KALLISTO_OUTPUT
cat $ADULT_RNASEQ/Adult_R1.fq $FETAL_RNASEQ/Fetal_R1.fq > Human_R1.fq
cat $ADULT_RNASEQ/Adult_R2.fq $FETAL_RNASEQ/Fetal_R2.fq > Human_R2.fq

mv Human_Combined_R1.fq Human_R1.fq
mv Human_Combined_R2.fq Human_R2.fq

#***************** STAR individual samples
#sample=(3005_11921 3005_12971FL 3005_12972FL)
#for i in ${sample[@]}; do
    #$run_STAR_human $i $FETAL_RNASEQ $MAPPED $REFERENCE
    #post_STAR_process $MAPPED
#done

#***************** STAR merged samples
# cd $MAPPED
#STAR --runMode alignReads --runThreadN 32 --genomeDir $REFERENCE/hg38 --readFilesIn $RAW/3005_11921_trimmed_r1.fq,$RAW/3005_12971FL_trimmed_r1.fq,#$RAW/3005_12972FL_trimmed_r1.fq \ 
#$RAW/3005_11921_trimmed_r2.fq,$RAW/3005_12971FL_trimmed_r2.fq,$RAW/3005_12972FL_trimmed_r2.fq \
#--outSAMtype BAM SortedByCoordinate \
#--chimSegmentMin 25 \
#--chimJunctionOverhangMin 20 \
#--chimOutType WithinBAM \
#--chimFilter banGenomicN \
#--chimOutJunctionFormat 1 \
#--twopassMode Basic \
#--twopass1readsN -1

#ls -alth *
#post_STAR_process fetal $MAPPED


################################################################################################
#************************************* RNASeq & IsoSeq 
source $FUNCTIONS/Mouse/WT8_IsoSeq3.1.2_Functions.sh
# run_kallisto <sample_prefix_output_name> <input_tofu_fasta> <merged_fastq_input_dir> <output_dir>
run_kallisto Fetal $FETAL_TOFU/fetalFL.sample.collapsed.filtered.rep.fa $FETAL_RNASEQ $FETAL_KALLISTO_OUTPUT
run_kallisto Adult $ADULT_TOFU/adult.sample.collapsed.filtered.rep.fa $ADULT_RNASEQ $ADULT_KALLISTO_OUTPUT
run_kallisto Human $HUMAN_TOFU/combinedFL.sample.collapsed.filtered.rep.fa $HUMAN_KALLISTO_OUTPUT $HUMAN_KALLISTO_OUTPUT


# run_post_isoseq3 <sample_prefix_input/output_name> <isoseq3_input_directory> <mapping_output_directory> <tofu_output_directory>
run_post_isoseq3 adult $ADULT_TOFU $ADULT_MAPPING $ADULT_TOFU
################################################################################################
#************************************* SQANTI2 [Function 7, 8] 
#run_sqanti2_QC_human <input_tofu_file> <input_tofu_prefix> <coverage=own_rnaseq/introplis> <input_kallisto_file> <output_dir> 
run_sqanti2_QC_human $FETAL_TOFU fetalFL.sample own_rnaseq $FETAL_KALLISTO_OUTPUT/Fetal.mod.abundance.tsv $FETAL_SQANTI2_output_dir_rnaseq
run_sqanti2_QC_human $FETAL_TOFU fetalFL.sample intropolis $FETAL_KALLISTO_OUTPUT/Fetal.mod.abundance.tsv $FETAL_SQANTI2_output_dir_intropolis
run_sqanti2_QC_human $FETAL_TOFU fetalFL.sample chess $FETAL_KALLISTO_OUTPUT/Fetal.mod.abundance.tsv $FETAL_SQANTI2_output_dir_chess
run_sqanti2_QC_human $ADULT_TOFU adult intropolis $ADULT_KALLISTO_OUTPUT/Adult.mod.abundance.tsv $ADULT_SQANTI2_output_dir_intropolis
run_sqanti2_QC_human $ADULT_TOFU adult chess $ADULT_KALLISTO_OUTPUT/Adult.mod.abundance.tsv $ADULT_SQANTI2_output_dir_chess
run_sqanti2_QC_human $HUMAN_TOFU combinedFL.sample chess $HUMAN_KALLISTO_OUTPUT/Human.mod.abundance.tsv $HUMAN_SQANTI2_output_dir_chess
run_sqanti2_QC_human $HUMAN_TOFU combinedFL.sample own_rnaseq $FETAL_KALLISTO_OUTPUT/Fetal.mod.abundance.tsv $HUMAN_SQANTI2_output_dir_fetal

# run_sqanti2_Filter <input/output_sqanti_dir> <sample_sqanti_prefix>
run_sqanti2_Filter fetalFL.sample $FETAL_SQANTI2_output_dir_rnaseq
run_sqanti2_Filter fetalFL.sample $FETAL_SQANTI2_output_dir_intropolis
run_sqanti2_Filter fetalFL.sample $FETAL_SQANTI2_output_dir_chess
run_sqanti2_Filter adult $ADULT_SQANTI2_output_dir_intropolis
run_sqanti2_Filter adult $ADULT_SQANTI2_output_dir_chess
run_sqanti2_Filter human $HUMAN_SQANTI2_output_dir_chess
run_sqanti2_Filter combinedFL.sample $HUMAN_SQANTI2_output_dir_fetal
