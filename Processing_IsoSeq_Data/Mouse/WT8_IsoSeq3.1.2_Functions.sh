################################################################################################
## Define functions for WT8IsoSeq3.1.2.sh
# 1) run_ccs <sample_prefix_output_name> <input_subreadset.xml_file> 
# 2) run_isoseq3_merge <sample_prefix_output_name> <working_directory> <input_ccs_file>
# 3) run_post_isoseq3 <sample_prefix_input/output_name> <isoseq3_input_directory> <mapping_output_directory> <tofu_output_directory>
# 4) run_star <list_of_samples> <J20/Tg4510_input_directory> <output_dir> <STAR_reference_input_directory>
# 5) mouse_merge_fastq <RNASEQ_input_dir> <Kallisto_output_dir> <sample_prefix_output_name>
# 6) run_kallisto <sample_prefix_output_name> <input_tofu_fasta> <merged_fastq_input_dir> <output_dir>
# 7) run_sqanti2_QC <input_tofu_prefix> <input_tofu_dir> <input_RNASEQ_dir> <input_KALLISTO_file> <output_dir> 
# 8) run_sqanti2_Filter <input/output_sample_prefix> <SQANTI2_input/output_dir>
################################################################################################
# 18/09/2019: updated script with isoseq3.2.1 (run_isoseq3.2.1_merge)
            # updated mouse gencode gtf file to main file, including redirected STAR_directory
            # included rarefaction curves from Liz's Annotation script
# 09/10/2019: run_post_isoseq3 with SQANTI 4.1 version
# 25/11/2019: incoporated all scripts into one for pipeline 
            # removed redundant run_official_isoseq3_merge(), run_isoseq3.2.1_merge (same as script in general dir)  
            # removed featurecounts_transcript
# 05-06/02/2020, 21/03/2020: SQANTI2 v7.4 update on SQANTI2 qc.py and filtering 




module load Miniconda2/4.3.21
################################################################################################
#*************************************  Isoseq3.1.2 [Function 1, 2]
# run_ccs <sample_prefix_output_name> <input_subreadset.xml_file>
# Aim: To generate CCS per sample 
# Output: <sample_prefix_output_name>.ccs.bam; <sample_prefix_output_name>_ccs_report.txt 
run_ccs(){
    source activate isoseq3
    ccs --version
    
    echo "Processing Sample $1 "
    echo "Processing $2"   
  
    if ccs --numThreads=32 --noPolish --minPasses=1 $2 $1.ccs.bam --reportFile $1_ccs_report.txt; then 
        echo "Processing Sample $1 successful"
    else 
        echo "Processing Sample $1 failed"
    fi
    source deactivate 
}


# run_isoseq3_merge <sample_prefix_output_name> <working_directory> <input_ccs_file>
# Aim: Pipeline for Isoseq3.1.2 (Lima, Refine, Polish) on merged samples 
# Prerequisite: 
    # DEFINE SAMPLES_NAMES list in working script
    # DEFINE RAW_DATA_SUBREADS in working script (path directories of subreadset.xml files)
    # DEFINE FASTA in working script (primer fasta)
run_isoseq3_merge(){
    source activate isoseq3
    ccs --version 
    lima --version
    isoseq3 --version

    echo "Processing isoseq3 merge of Sample $1"

    cd $2               
    # Define variable "Merge_Samples" as a list of all samples, in order to find the specified ccs.bam (for dataset create ConsensusReadSet) 
    # Define variable "Raw_Data" as a list of path_directory of all subread files (for dataset create SubreadSet)
    Merge_Samples=$(echo ${SAMPLES_NAMES[@]})
    Raw_Data=$(echo ${RAW_DATA_SUBREADS[@]})

    # Define variable "all_ccs_bams" for merged path-directory of all ccs samples (for dataset create ConsensusReadSet)
    echo "Merging ccs of samples $Merge_Samples"
    all_ccs_bams=$(
        for i in ${Merge_Samples[@]}; do
            ccs_bam_name=$(find $3 -name "*.ccs.bam" -exec basename \{} \; | grep ^$i )
            ccs_bam=$(find $3 -name "*.ccs.bam" | grep "$ccs_bam_name" )
            echo $ccs_bam
        done
    )
    echo "Merging the following ccs.bams"
    echo ${all_ccs_bams[@]}
    # dataset create --type ConsensusReadSet <output.ccs.consensusreadset.xml> <input_ccs_bams>
    dataset create --type ConsensusReadSet $1_ccs.consensusreadset.xml $all_ccs_bams

    echo "Merging the following subreads.bam for Subreadset.xml $Raw_Data"
    # dataset create --type SubreadSet <output.subreadset.merged.xml> <input_subreadset.xml>
    dataset create --type SubreadSet $1.subreadset.xml $Raw_Data

    echo "Processing LIMA for sample $1"
    # lima <input.ccs.merged.consensusreadset.xml> <input.primerfasta> <output.fl.bam> 
    lima $1_ccs.consensusreadset.xml $FASTA $1.fl.bam --isoseq --dump-clips
    # merging lima.bam files based on the sample names saved as samples_merged on working script
    # dataset create --type ConsensusReadSet --name <output_name> <output.consensusreadset.xml> <input_lima_bams>
    lima_bams=$(ls *primer_5p*3p.bam)
    dataset create --type ConsensusReadSet --name $1_FL $1_FL.consensusreadset.xml $lima_bams

    echo "Processing Isoseq3 refine, cluster and polish"
    # refine --require-polya <input.lima.consensusreadset.xml> <input.primer.fasta> <output.flnc.bam> 
    time isoseq3 refine --require-polya $1_FL.consensusreadset.xml $FASTA $1.flnc.bam
    # cluster <input.flnc.bam> <output.unpolished.bam>
    time isoseq3 cluster $1.flnc.bam $1.unpolished.bam -j 32 --log-file "$1_cluster.log"
    # polish <input.unpolished.bam> <input.subreadset.merged.xml> <output.polished.bam> 
    time isoseq3 polish -j 32 $1.unpolished.bam $1.subreadset.xml $1.polished.bam --log-file "$1_polish.log"
    gunzip *.gz

    source deactivate
}


################################################################################################
#************************************* Post_Isoseq3 (Minimap2, Cupcake) [Function 3] 
# run_post_isoseq3 <sample_prefix_input/output_name> <isoseq3_input_directory> <mapping_output_directory> <tofu_output_directory>
# Prerequisite: mm10 cage peak 
run_post_isoseq3(){
    
    CUPCAKE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake
    SQANTI2_dir=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/SQANTI2
    REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019
    ANNOTATION=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/annotation
    SEQUENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/sequence

    source activate sqanti2

    # convert between fasta and fastq for downstream process
    echo "fasta2fastq conversion"
    python $SEQUENCE/fa2fq.py $2/$1.polished.hq.fasta 
    
    samtools --version # echo version
    echo "Minimap2 version:" $(minimap2 --version) # echo version

    echo "Processing Sample $1 for Minimap2 and sort"
    cd $3 #cd to $MAP directory for output
    minimap2 -t 30 -ax splice -uf --secondary=no -C5 -O6,24 -B4 $REFERENCE/mm10.fa $2/$1.polished.hq.fastq > $1.sam 2> $1.map.log
    samtools sort -O SAM $1.sam > $1.sorted.sam
    
    head $CUPCAKE/README.md
    echo "Processing Sample $1 for ToFU collapse"
    cd $4 #cd to $TOFU directory for output
    if python $CUPCAKE/cupcake/tofu/collapse_isoforms_by_sam.py --input $2/$1.polished.hq.fastq --fq -s $3/$1.sorted.sam --dun-merge-5-shorter -o $1 \
    2> $1.collapse.log; then 
        echo "Processing Sample $1 for collapse successful"
    else 
        echo "Processing Sample $1 for collapse failed"
    fi

    python $CUPCAKE/post_isoseq_cluster/isoseq3_make_cluster_report.py $2/$1.polished.bam 2> $1.make.cluster.log 
    mv cluster_report.csv $1.cluster_report.csv
    
    if python $CUPCAKE/cupcake/tofu/get_abundance_post_collapse.py $1.collapsed $1.cluster_report.csv 2> $1.abundance.log; then 
        echo "Processing Sample $1 for getting abundance successful"
    else 
        echo "Processing Sample $1 for getting abundance failed"
    fi

    if python $CUPCAKE/cupcake/tofu/filter_away_subset.py $1.collapsed 2> $1.filter.log; then 
        echo "Processing Sample $1 for filtering successful"
    else 
        echo "Processing Sample $1 for filtering failed" 
    fi

    if python $CUPCAKE/cupcake/tofu/fusion_finder.py --input $2/$1.polished.hq.fastq --fq -s $3/$1.sorted.sam -o $1.fusion \
    --cluster_report_csv $1.cluster_report.csv 2> $1.fusion.log ; then 
        echo "Processing Sample $1 for gene fusion successful"
    else 
        echo "Processing Sample $1 for gene fusion failed"
    fi 

    seqtk seq -a $1.collapsed.filtered.rep.fq > $1.collapsed.filtered.rep.fa
    head *log

    source deactivate
}


################################################################################################
#************************************* RNAseq [Function 4, 5] 
# run_star <list_of_samples> <J20/Tg4510_input_directory> <output_dir> <STAR_reference_input_directory>
# Aim: Individually align RNASeq filtered fastq files to STAR-indexed genome using STAR, and subsequently input SJ.out.bed file into SQANTI2
run_star(){
    source activate sqanti2
    echo "STAR Version"
    STAR --version
    # samtools version: Version: 1.9 (using htslib 1.9)

    # extract official sample name from all_filtered directory 
    # extract only files with "fastq.filtered", beginning with sample name, and R1/R2

    # cd to $J20/Tg4510_input_directory
    F_name=$(find $2 -name "*fastq.filtered" -exec basename \{} \; | grep ^$1 | grep "R1" )
    R_name=$(find $2 -name "*fastq.filtered" -exec basename \{} \; | grep ^$1 | grep "R2" )
    # save path directory of files as variable for later mapping
    F_File=$(find $2 -name "$F_name")
    R_File=$(find $2 -name "$R_name")
    
    # cd to $WKD
    cd $3
    if [ -f $1.bam ]; then 
        echo "$1.bam file already exists; STAR Mapping not needed on Sample $1"
    else
        echo "Processing Sample $1 for STAR"
        echo "Processing Forward Reads: $F_File"
        echo "Processing Reverse Reads: $R_File"
   
     # Parameters according to https://github.com/jennylsmith/Isoseq3_workflow/blob/master/shell_scripts/8__STAR_Junctions_ShortReads.sh
    # 2-pass mode alignment with chimeric read detection
	# at least 25 bp of one read of a pair maps to a different loci than the rest of the read pair
	# require 20 pb on either side of chimeric junction
	# include chimeric reads in the output BAM
	# don't include chimeras that map to reference genome contig with Ns
    # --outSAMtype BAM SortedByCoordinate, output sorted by coordinate Aligned.sortedByCoord.out.bam file, similar to samtools sort command.
    # note STAR indexed with genocde vM22 primary assembly annotation gtf therefore no need to include into command line, otherwise create additional folders
        STAR --runMode alignReads --runThreadN 32 --genomeDir $4/STAR_main \
		--readFilesIn $F_File $R_File \
        --outSAMtype BAM SortedByCoordinate \
        --chimSegmentMin 25 \
		--chimJunctionOverhangMin 20 \
		--chimOutType WithinBAM \
		--chimFilter banGenomicN \
		--chimOutJunctionFormat 1 \
		--twopassMode Basic \
		--twopass1readsN -1 #use all reads
        #--readFilesCommand zcat \
        #--sjdbGTFfile $4/gencode.vM22.primary_assembly.annotation.gtf \

        # to remove duplicates between samples 
        picard MarkDuplicates INPUT=$1.sorted.bam OUTPUT=$1.sorted.nodup.bam METRICS_FILE=$1.dup.txt VALIDATION_STRINGENCY=LENIENT REMOVE_DUPLICATES=true \
        2> $1.PicardDup.log
        
        # rename output files
        mv Aligned.sortedByCoord.out.bam $1.sorted.bam
        mv Log.final.out $1.Log.final.out 
        mv Log.out $1.Log.out
        mv Log.progress.out $1.Log.progress.out
        mv SJ.out.tab $1.SJ.out.bed      
    fi  
    source deactivate
}

# mouse_merge_fastq <RNASEQ_input_dir> <Kallisto_output_dir> <sample_prefix_output_name>
# Aim: Merge forward and reverse of all RNASeq filtered fastq, for further downstream alignment to IsoSeq Tofu output using Kallisto
mouse_merge_fastq(){
    R1_READS=($(
        for i in ${SAMPLES_NAMES[@]}; do 
            F_name=$(find $1 -name "*fastq.filtered" -exec basename \{} \; | grep ^$i | grep "R1" )
            F_File=$(find $1 -name "$F_name")
            echo $F_File
        done
    ))

    R2_READS=($(
        for i in ${SAMPLES_NAMES[@]}; do 
            R_name=$(find $1 -name "*fastq.filtered" -exec basename \{} \; | grep ^$i | grep "R2" )
            R_File=$(find $1 -name "$R_name")
            echo $R_File
        done
    ))

    R1_READS_MERGE=$(echo "${R1_READS[@]}")
    R2_READS_MERGE=$(echo "${R2_READS[@]}")

    echo "Processing R1 READS"
    echo $R1_READS_MERGE
    echo "Processing R2 READS"
    echo $R2_READS_MERGE

    cd $2
    cat $R1_READS_MERGE > $3_R1.fq
    cat $R2_READS_MERGE > $3_R2.fq
} 


################################################################################################
#************************************* RNASeq & IsoSeq [Function 6] 
# run_kallisto <sample_prefix_output_name> <input_tofu_fasta> <merged_fastq_input_dir> <output_dir>
# Aim: Align merged RNASeq fastq files to IsoSeq Tofu output fasta using Kallisto 
# Prerequisite:
    # run mouse_merge_fastq/any equivalent merging of all RNASeq fastq files (note sample_prefix_output name is the same)
# Output: <output_prefix>.mod.abundance.tsv for input into Sqanti_qc.py (not modified script to take in space delimited rather than tab file)
run_kallisto(){
    source activate sqanti2

    echo "Processing Kallisto for $1"
    cd $4
    kallisto version
    time kallisto index -i $1_Kallisto.idx $2 2> $1_Kallisto.index.log
    time kallisto quant -i $4/$1_Kallisto.idx --fr-stranded $3/$1_R1.fq --rf-stranded $3/$1_R2.fq -o $4 2> $1_Kallisto.quant.log
    mv abundance.tsv $1.abundance.tsv
    
    # problem: retained co-ordinates, which does not input well into SQANTI2
    echo "Kallisto original $1.abundance.tsv"
    head $1.abundance.tsv
    # solution: retain the PB.ID
    while read line ; do
	    first=$( echo "$line" |cut -d\| -f1 ) # each read line, and remove the first part i.e. PacBio ID
	    rest=$( echo "$line" | cut -d$'\t' -f2-5 ) #save the remaining columns
	    echo $first $rest # concatenate
    done < $4/$1.abundance.tsv > $4/$1.temp.abundance.tsv

    header=$(head -n 1 $4/$1.abundance.tsv)
	sed -i '1d' $4/$1.temp.abundance.tsv # remove header of temp.file to be replaced
    echo $header > foo
	cat foo $4/$1.temp.abundance.tsv > $4/$1.mod.abundance.tsv 

    echo "Kallisto $1.mod.abundance.tsv"
    head $4/$1.mod.abundance.tsv
    rm $1.temp.abundance.tsv
	rm foo

    source deactivate
}


################################################################################################
#************************************* SQANTI2 [Function 7, 8] 
# run_sqanti2_QC <input_tofu_prefix> <input_tofu_dir> <input_RNASEQ_dir> <input_KALLISTO_file> <output_dir> 
run_sqanti2_QC(){

    source activate sqanti2_py3

    SQANTI2_DIR=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/SQANTI2
    CUPCAKE_SEQUENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/sequence
    REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019
    CAGE_DIR=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019/CAGE

    echo "Processing Sample $1 for SQANTI2 QC"
    cd $5       
    
    # copy STAR output SJ.out.bed files 
    SJ_OUT_BED=($(
        for rnaseq in ${SAMPLES_NAMES[@]}; do
            name=$(find $3 -name "*SJ.out.bed" -exec basename \{} \; | grep ^$rnaseq)
            File=$(find $3 -name "$name")
            echo $File
        done
        ))

    for file in ${SJ_OUT_BED[@]}; do cp $file . ;done

    export PYTHONPATH=$PYTHONPATH:$CUPCAKE_SEQUENCE
    python $SQANTI2_DIR/sqanti_qc2.py -v

    if python $SQANTI2_DIR/sqanti_qc2.py -t 30 \
        $2/$1.collapsed.filtered.rep.fa \
        $REFERENCE/gencode.vM22.annotation.gtf  \
        $REFERENCE/mm10.fa \
        --cage_peak $CAGE_DIR/mm10.cage_peak_phase1and2combined_coord.bed \
        --coverage "./*SJ.out.bed" \
        --polyA_motif_list $SQANTI2_DIR/../human.polyA.list.txt \
        --fl_count $2/$1.collapsed.abundance.txt \
        --expression $4 \
        &> $1.sqanti.qc.log; then
        echo "Processing Sample $1 for SQANTI2 successful" 
        samtools view -S -b $1.collapsed.filtered.rep_corrected.sam > $1.collapsed.filtered.rep.renamed_corrected.bam 
    else 
        echo "Processing Sample $1 for SQANTI2 failed" 
    fi

    # remove temp SJ.out bed files
    rm *SJ.out.bed 

    source deactivate    

}

# run_sqanti2_Filter <input/output_sample_prefix> <SQANTI2_input/output_dir>
run_sqanti2_Filter(){

    source activate sqanti2_py3

    SQANTI2_DIR=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/SQANTI2
    CUPCAKE_SEQUENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Post_Isoseq3/cDNA_Cupcake/sequence
  
    echo "Processing Sample $2 for SQANTI2 filter"
    cd $2

    export PYTHONPATH=$PYTHONPATH:$CUPCAKE_SEQUENCE
    python $SQANTI2_DIR/sqanti_qc2.py -v

    if python $SQANTI2_DIR/sqanti_filter2.py \
        $2/$1.collapsed.filtered.rep_classification.txt \
        $2/$1.collapsed.filtered.rep_corrected.fasta  \
        $2/$1.collapsed.filtered.rep_corrected.gtf \
        -a 0.6 -c 3 &> $1.sqanti.filter.log; then
        echo "Processing Sample $1 for SQANTI2 filter successful"
    else
        echo "Processing Sample $1 for SQANTI2 filter failed"
    fi

    source deactivate    
}

################################################################################################
