#!/bin/sh
#PBS -V # export all environment variables to the batch job.
#PBS -q mrchq # submit to queue
#PBS -l walltime=144:00:00 # Maximum wall time for the job in hours:minutes:seconds
#PBS -A Research_Project-MRC148213 # research project to submit under.
#PBS -l procs=32 # specify number of processors.
#PBS -m e -M sl693@exeter.ac.uk # email me at job completion

# 04/07/2019: run individual ccs on WT8 samples 
# 04/07/2019: run STAR individual on WT8 samples 
# 04-06/07/2019: run_isoseq3_merge on WT8 samples
# 12/06/2019: run_post_isoseq3 on WT8 samples 
# 17/07/2019: reduced intrapriming for SQANTI2 filter to 0.6
# 06/08/2019: rerun post_isoseq3 on WT8 samples by mapping to vM22 gencode gtf, updated v3.7 SQANTI2 QC, 0.6 intrapriming SQANTI2 filter 
# 25/11/2019: split post_isososeq3 to pre- and post-SQANTI2, and rerun SQANTI2 in version 4.1 with Kallisto_RNASEQ input 
# 05/02/2020: SQANTI2 v7 update
# 21/03/2020: SQANTI2 v7.4 update 

#************************************* DEFINE GLOBAL VARIABLES
# setting names of directory outputs
WholeTranscriptome=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/WholeTranscriptome
WKD=$WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2

cd $WKD
#mkdir Isoseq3_WKD CCS MAP TOFU SQANTI2 ALIGNQC FEATURECOUNT
Isoseq3_WKD=$WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/Isoseq3_WKD
CCS=$WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/CCS
MAP=$WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/MAP
TOFU=$WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/TOFU
SQANTI2=$WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/SQANTI2_v7/WT8
KALLISTO=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/KALLISTO

# sourcing functions script and input directories
FUNCTIONS=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Scripts/human_mouse_isoseq/Isoseq_Post/Merge/Mouse
source $FUNCTIONS/WT8_IsoSeq3.1.2_Functions.sh
RAW_DIR=$WholeTranscriptome/Individual/Isoseq3/rawdata
FASTA=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Scripts/general/primer.fasta
REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019 # for mapping
RNASeq_Filtered=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/RNASeq/all_filtered
STAR_dir=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/RNASeq/MAPPED/WT8


################################################################################################
#************************************* DEFINE LOCAL VARIABLES
SAMPLES_NAMES=(Q21 O23 S23 K17 C21 B21 M21 K23)

RAW_DATA=(
#1. Q21_Tg4510_WT_2mos
$RAW_DIR/m54082_180607_173058.subreadset.xml
#2. O23_Tg4510_WT_8mos
$RAW_DIR/m54082_190401_165425.subreadset.xml
#3. S23_Tg4510_WT_8mos
$RAW_DIR/m54082_190403_135102.subreadset.xml
#4. K17_Tg4510_WT_2mos
$RAW_DIR/m54082_190405_063832.subreadset.xml
#5. C21_J20_WT_6mos
$RAW_DIR/m54082_180816_074627.subreadset.xml
#6. B21_J20_WT_12months
$RAW_DIR/m54082_190303_070925.subreadset.xml
#11. M21_Tg4510_WT_2mos
$RAW_DIR/m54082_190430_163756.subreadset.xml
#12. K23_Tg4510_WT_8mos
$RAW_DIR/m54082_190524_145911.subreadset.xml
)

RAW_DATA_SUBREADS=(
#1. Q21_Tg4510_WT_2mos
$RAW_DIR/m54082_180607_173058.subreads.bam
#2. O23_Tg4510_WT_8mos
$RAW_DIR/m54082_190401_165425.subreads.bam
#3. S23_Tg4510_WT_8mos
$RAW_DIR/m54082_190403_135102.subreads.bam
#4. K17_Tg4510_WT_2mos
$RAW_DIR/m54082_190405_063832.subreads.bam
#5. C21_J20_WT_6mos
$RAW_DIR/m54082_180816_074627.subreads.bam
#6. B21_J20_WT_12months
$RAW_DIR/m54082_190303_070925.subreads.bam
#11. M21_Tg4510_WT_2mos
$RAW_DIR/m54082_190430_163756.subreads.bam
#12. K23_Tg4510_WT_8mos
$RAW_DIR/m54082_190524_145911.subreads.bam
)


################################################################################################
#************************************* Isoseq3.1.2 and Post_Isoseq3 [Function 1,2,3]
## 1) run_ccs <sample_prefix_output_name> <input_subreadset.xml_file> 
#cd $CCS
#count=0
#for i in "${SAMPLES_NAMES[@]}"; do
    #Raw_Data_Input=${RAW_DATA[count]}
    #run_ccs $i $Raw_Data_Input  
    #count=$((count+1))
#done

## 2) run_isoseq3_merge <sample_prefix_output_name> <working_directory> <input_ccs_file>
## in function script: samples=$(echo ${SAMPLES_NAMES[@]})
## in function script: rawdata=$(echo ${RAW_DATA[@]})
# run_isoseq3_merge WT8IsoSeq $Isoseq3_WKD $CCS

## 3) run_post_isoseq3 <sample_prefix_input/output_name> <isoseq3_input_directory> <mapping_output_directory> <tofu_output_directory>
run_post_isoseq3 WT8IsoSeq $Isoseq3_WKD $MAP $TOFU 


################################################################################################
#************************************* RNAseq [Function 4, 5] 
## 4) run_star <list_of_samples> <J20/Tg4510_input_directory> <output_dir> <STAR_reference_input_directory>
#for i in "${SAMPLES_NAMES[@]}"; do
    #run_star $i $RNASeq_Filtered $STAR_dir $REFERENCE
#done 

## 5) mouse_merge_fastq <RNASEQ_input_dir> <Kallisto_output_dir> <sample_prefix_output_name>
mouse_merge_fastq $RNASeq_Filtered $KALLISTO WT8


################################################################################################
#************************************* RNASeq & IsoSeq [Function 6] 
## 6) run_kallisto <sample_prefix_output_name> <input_tofu_fasta> <merged_fastq_input_dir> <output_dir>
run_kallisto WT8 $TOFU/WT8IsoSeq.collapsed.filtered.rep.fa $KALLISTO $KALLISTO


################################################################################################
#************************************* SQANTI2 [Function 7, 8] 
## 7) run_sqanti2_QC <input_tofu_prefix> <input_tofu_dir> <input_RNASEQ_dir> <input_KALLISTO_file> <output_dir> 
run_sqanti2_QC WT8IsoSeq $TOFU $STAR_dir $KALLISTO/WT8.mod.abundance.tsv $SQANTI2

## 8) run_sqanti2_Filter <input/output_sample_prefix> <SQANTI2_input/output_dir>
run_sqanti2_Filter WT8IsoSeq $SQANTI2
