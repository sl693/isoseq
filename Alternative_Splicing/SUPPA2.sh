#!/bin/sh
#PBS -V # export all environment variables to the batch job.
#PBS -q mrchq # submit to queue
#PBS -l walltime=144:00:00 # Maximum wall time for the job in hours:minutes:seconds
#PBS -A Research_Project-MRC148213 # research project to submit under.
#PBS -l procs=32 # specify number of processors.
#PBS -m e -M sl693@exeter.ac.uk # email me at job completion

# 05/02/2020: SUPPA generate events on mouse dataset 
# 09/03/2020: SUPPA generate events on human datasets 
# 03/04/2020: SUPPA generate events on human_chained datasets 
# 08/04/2020: SUPPA generate -ioi on human_chained datasets
# 20/04/2020: SUPPA generate events on human_chained datsets using specified parameters
# 15/05/2020: repeat SUPPA generate events on human datasets using Aaron's SQ7.4 files (noticed problem with previous classification file), and included in python script 
# 12/08/2020: SUPPA generate events on fetal brain datasets 	

module load Miniconda2 
source activate sqanti2_py3 
#conda install -c bioconda suppa

#************************************* DEFINE GLOBAL VARIABLES
sl693=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693
FUNCTIONS=/gpfs/ts0/home/sl693/Scripts/human_mouse_isoseq/DownStream/SUPPA
mouse_SUPPA2_output_dir=$sl693/WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/SUPPA2
mouse_SQANTI2_input_dir=$sl693/WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/SQANTI2

human_SQANTI2_input_dir=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/SQ2_v7_4 #Aaron's SQ7.4 files 
human_output_SUPPA=$sl693/Isoseq_Paper/Human_SUPPA

#************************************* Suppa generate Events 
# Loop through human dataset 
human_input_sqanti_gtf=($human_SQANTI2_input_dir/combinedFL.sample.collapsed.filtered.rep_classification.filtered_lite.gtf 
$human_SQANTI2_input_dir/adult.sample.collapsed.filtered.rep_classification.filtered_lite.gtf 
$human_SQANTI2_input_dir/fetalFL.sample.collapsed.filtered.rep_classification.filtered_lite.gtf
$human_SQANTI2_input_dir/fetalHIP.sample.collapsed.filtered.rep_classification.filtered_lite.gtf
$human_SQANTI2_input_dir/fetalSTR.sample.collapsed.filtered.rep_classification.filtered_lite.gtf
)

human_suppa2_output_file=(combined_human adult fetalCTX fetalHIP fetalSTR)

human_dataset=(combinedFL adult fetalCTX fetalHIP fetalSTR)
for i in {0..4}; do 
    echo "Processing with ${human_input_sqanti_gtf[i]}"
    echo "Output: ${human_suppa2_output_file[i]}"
    cd $human_output_SUPPA
    suppa.py generateEvents -i ${human_input_sqanti_gtf[i]} -o ${human_suppa2_output_file[i]} --pool-genes -f ioe -e SE MX FL SS RI 2> ${human_dataset[i]}.map.log
done 

suppa.py generateEvents -i $human_SQANTI2_input_dir/combinedFL.sample.collapsed.filtered.rep_classification.filtered_lite.gtf -o combined_strict --pool-genes -b S -f ioe -e SS 

# mouse dataset 
suppa.py generateEvents -i $mouse_SQANTI2_input_dir/WT8IsoSeq.collapsed.filtered.rep_classification.filtered_lite.gtf -o $mouse_SUPPA2_output_dir/WT8IsoSeq --pool-genes -f ioe -e SE MX FL SS RI

#************************************* Apply Suppa2_output_mod_updated.py script 
# Python corrected files 
#Suppa2_output_mod_updated.py <name> <SUPPA2_input_dir> <SQANTI2_classification_file>
python $FUNCTIONS/Suppa2_output_mod_updated.py "adult" $human_output_SUPPA $human_SQANTI2_input_dir/adult.sample.collapsed.filtered.rep_classification.filtered_lite_classification.txt

python $FUNCTIONS/Suppa2_output_mod_updated.py "combined_human" $human_output_SUPPA $human_SQANTI2_input_dir/combinedFL.sample.collapsed.filtered.rep_classification.filtered_lite_classification.txt

python $FUNCTIONS/Suppa2_output_mod_updated.py "combined_strict" $human_output_SUPPA $human_SQANTI2_input_dir/combinedFL.sample.collapsed.filtered.rep_classification.filtered_lite_classification.txt

python $FUNCTIONS/Suppa2_output_mod_updated.py "fetalCTX" $human_output_SUPPA $human_SQANTI2_input_dir/fetalFL.sample.collapsed.filtered.rep_classification.filtered_lite_classification.txt

python $FUNCTIONS/Suppa2_output_mod_updated.py "fetalHIP" $human_output_SUPPA $human_SQANTI2_input_dir/fetalHIP.sample.collapsed.filtered.rep_classification.filtered_lite_classification.txt

python $FUNCTIONS/Suppa2_output_mod_updated.py "fetalSTR" $human_output_SUPPA $human_SQANTI2_input_dir/fetalSTR.sample.collapsed.filtered.rep_classification.filtered_lite_classification.txt

python $FUNCTIONS/Suppa2_output_mod_updated.py "WT8IsoSeq" $mouse_SUPPA2_output_dir $mouse_SQANTI2_input_dir/WT8IsoSeq.collapsed.filtered.rep_classification.filtered_lite_classification.txt