#!/bin/sh
#PBS -V # export all environment variables to the batch job.
#PBS -q mrcq # submit to the serial queue
#PBS -l walltime=144:00:00 # Maximum wall time for the job.
#PBS -A Research_Project-MRC148213
#PBS -l procs=1 # specify number of processors.
#PBS -m e -M sl693@exeter.ac.uk # email me at job completion

# 28/08/2019: Blast Human novel genes from classification file to Mouse novel genes for comparison
# 30/04/2020: Repeat Blast on  human novel genes to mouse novel genes, but using Sqantiv7.4
# 01/08/2020: Blast novel genes across respective genome 


#************************************************************Define Global Variables

Human=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/SQ2_v7_4
Mouse=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/SQANTI2_v7/WT8
FUNCTIONS=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Scripts/general/Post_IsoSeq
REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019
HUMAN2MOUSE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Isoseq_Paper/Novel_Genes/Human2Mouse
BLAST_ACROSS_GENOME=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Isoseq_Paper/Novel_Genes/AcrossGenome

#************************************************************Define functions
# Extract_PBIDs <input_classification_file>.txt <input_dir_path> <output_dir_path>
Extract_PBIDs(){
  cd $2

  # print PacBio Id and remove first row 
  awk '{ print $1 }' $1.txt >> $1_PBIds.txt 
  cat $1_PBIds.txt | sed 's/"//g' | sed 's/^/>/' >> $3/$1_PBIds.txt
}

# Grep_Fasta <prefix_PBIds.txt> <SQANTI2_filtered.fasta> <output_name.fasta> <output_dir_path>
Grep_Fasta(){
  # https://askubuntu.com/questions/976414/how-to-grep-sequence-of-fasta-using-list-of-ids-in-another-file
  grep -x -F -A 1 -f $1_PBIds.txt $2 >> $4/$3
  sed '/--/d' $4/$3 >> $4/Final_$3 #remove hyphens in-between lines
  rm $4/$3
}

Grep_Gtf(){  
  while read p; do
    grep "$p" $SQANTI2/WT8IsoSeq.collapsed.filtered.rep.renamed_corrected.gtf
  done <$WKD/Mouse_Novel_PBIds.txt >> $WKD/WT8IsoSeq_NOVEL.collapsed.filtered.rep.renamed_corrected.gtf
}

#Blast_seq2seq <working directory> <sample.fasta.for.db> <output_db_name> <sample.fasta.for.blast> <blast.output.name> <build>
Blast_seq2seq(){

  ### Prepared Reference Genome for BLAST
  module load Miniconda2
  source activate nanopore
  cd $1
  if [[ $6 == "build" ]]; then 
	echo "Create blast database with fasta sequence: $2"
	makeblastdb -in $2 -dbtype nucl -out $3
	echo "Output file successfully generated: $2"

	### Blast Probes to Reference Genome
	# https://angus.readthedocs.io/en/2016/running-command-line-blast.html
	# http://envgen.nox.ac.uk/bioinformatics/documentation/blast+/user_manual.pdf
	echo "Blast Probes to indexed database with fasta sequence: $3"
	blastn -query $4 -db $3 -out $5 -outfmt 6 -evalue 1e-5 
	#  -outfmt "7 qacc sacc evalue qstart qend sstart send" -evalue 1e-5  

	# column headers:https://molevol.mbl.edu/index.php/BLAST_UNIX_Tutorial
	#echo -e "Query_ID\Subject_ID\%_Identity\alignment_length\mismatches\gap\q.start\q.end\s.start\s.end\e_value\bit_score" | cat - $5 > Final_$5
  else 
	echo "Blast database with fasta sequence: $4"
	blastn -query $4 -db $3 -out $5 -outfmt 6 -evalue 1e-5 
  fi  
	  
}

#*********************************************************** Run functions
# Extract_PBIDs <input_classification_file>txt <input_dir_path> <output_dir_path>
# Grep_Fasta <prefix_PBIds.txt> <SQANTI2_filtered.fasta> <output_name.fasta> <output_dir_path>

cd $HUMAN2MOUSE
module load Miniconda2 
source activate nanopore 
Rscript Create_Novel_Genes_Classification.R
source deactivate 

Extract_PBIDs Combined_Human_Novel_classification . .
Grep_Fasta Combined_Human_Novel_classification $Human/combinedFL.sample.collapsed.filtered.rep_corrected.fasta Combined_Human_Novel.fasta .

Extract_PBIDs Mouse_Novel_classification . . 
Grep_Fasta Mouse_Novel_classification $Mouse/WT8IsoSeq.collapsed.filtered.rep_corrected.fasta Mouse_Novel.fasta .

# grep PB.15851.3 -A 1 $SQANTI2/WT8IsoSeq.collapsed.filtered.rep_classification.filtered_lite.fasta
# grep PB.15851.3 -A 1 $SQANTI2/WT8IsoSeq.collapsed.filtered.rep.renamed_corrected.fasta

#Blast_seq2seq <working directory> <sample.fasta.for.db> <output_db_name> <sample.fasta.for.blast> <blast.output.name>
Blast_seq2seq . Final_Mouse_Novel.fasta Mouse_Novel.db Final_Combined_Human_Novel.fasta Human2Mouse.blast.txt build


#*********************************************************** BLAST across genome 
# novelGene_E2F3_AS (human) matched with mouse
# note using the corrected.fasta for mouse rather than SQANTI2 filtered fasta as cut fasta sequence (shouldn't make a difference whether corrected.fasta or filtered however) 
cd $BLAST_ACROSS_GENOME
grep PB.21939.1 -A 1 /gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/SQ2_v7_4/combinedFL.sample.collapsed.filtered.rep_classification.filtered_lite.renamed.fasta > E2F3_Human.fasta 
grep PB.3680.1 -A 1 $Mouse/WT8IsoSeq.collapsed.filtered.rep_corrected.fasta > E2F3_Mouse.fasta 


Blast_seq2seq . $REFERENCE/mm10.fa mm10.db E2F3_Mouse.fasta E2F3_Mouse_Genome_blast.txt build
Blast_seq2seq . $REFERENCE/hg38.fa hg38.db E2F3_Human.fasta E2F3_Human_Genome_blast.txt build

Blast_seq2seq . $REFERENCE/mm10.fa mm10.db $HUMAN2MOUSE/Final_Mouse_Novel.fasta Mouse_Novel_Mouse_Genome_blast.txt not_build 
Blast_seq2seq . $REFERENCE/hg38.fa hg38.db $HUMAN2MOUSE/Final_Combined_Human_Novel.fasta Human_Novel_Human_Genome_blast.txt not_build 

