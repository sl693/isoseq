#!/bin/bash
#SBATCH --export=ALL # export all environment variables to the batch job
#SBATCH -D . # set working directory to .
#SBATCH -p mrcq # submit to the parallel queue
#SBATCH --time=10:00:00 # maximum walltime for the job
#SBATCH -A Research_Project-MRC148213 # research project to submit under
#SBATCH --nodes=1 # specify number of nodes
#SBATCH --ntasks-per-node=16 # specify number of processors per node
#SBATCH --mail-type=END # send email at job completion
#SBATCH --mail-user=sl693@exeter.ac.uk # email address

# 14/08/2020: Identify novel antisense genes in human and mouse, and the subset of which overlap with exonic regions of other genes using bedtools
    # Note Bedtools intersect requires input as bed file, thus subset gtf and then convert gtf to bed 

#************************************* DEFINE GLOBAL VARIABLES
Human=/gpfs/mrc0/projects/Research_Project-MRC148213/ISOSEQ/SQ2_v7_4
Mouse=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/WholeTranscriptome/WT8_ISOSEQ/IsoSeq3.1.2/SQANTI2_v7/WT8
ANTISENSE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Isoseq_Paper/Novel_Genes/Antisense
REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019


#************************************* 1. Subset gtf to only antisense novel genes 
module load Miniconda2 
source activate nanopore 

Extract_PBIDs(){
  cd $2

  # print PacBio Id and remove first row 
  awk '{ print $1 }' $1.txt >> $1_PBIds.txt
}

cd $ANTISENSE
# Manually create Rscript external to this bash script to generate list of IDs of antisense novel genes 
Rscript Create_Novel_Genes_Antisense_Classification.R
Extract_PBIDs Combined_Human_Novel_Antisense_classification . . 
Extract_PBIDs Mouse_Novel_Antisense_classification . . 
grep -f Combined_Human_Novel_Antisense_classification_PBIds.txt $Human/combinedFL.sample.collapsed.filtered.rep_corrected.gtf  > combined.NovelGenes.Antisense.gtf
grep -f Mouse_Novel_Antisense_classification_PBIds.txt $Mouse/WT8IsoSeq.collapsed.filtered.rep_corrected.gtf > WT8IsoSeq.NovelGenes.Antisense.gtf

# Check grepped the correct number of novel genes 
awk '{ if($3 == "transcript") { print }}' combined.NovelGenes.Antisense.gtf | wc -l
awk '{ if($3 == "transcript") { print }}' WT8IsoSeq.NovelGenes.Antisense.gtf | wc -l


#************************************* 2. Extract gtf of other detected genes  
# All genes minus antisense 
Rscript Create_no_Antisense_Classification.R
Extract_PBIDs Mouse_Novel_No_Antisense_classification . . 
Extract_PBIDs Combined_Human_No_Antisense_classification . . 
grep -f Combined_Human_No_Antisense_classification_PBIds.txt $Human/combinedFL.sample.collapsed.filtered.rep_corrected.gtf  > combined.No_Antisense.gtf
grep -f Mouse_Novel_No_Antisense_classification_PBIds.txt $Mouse/WT8IsoSeq.collapsed.filtered.rep_corrected.gtf > WT8IsoSeq.No_Antisense.gtf


#************************************* 3. Convert gtf to bed 
# conda install -c bioconda bedops
gtf2bed < combined.NovelGenes.Antisense.gtf >> combined.NovelGenes.Antisense.bed
gtf2bed < $Human/combinedFL.sample.collapsed.filtered.rep_corrected.gtf >> combined.collapsed.bed 

gtf2bed < combined.No_Antisense.gtf >> combined.No_Antisense.bed 
gtf2bed < WT8IsoSeq.No_Antisense.gtf >> WT8IsoSeq.No_Antisense.bed

gtf2bed < WT8IsoSeq.NovelGenes.Antisense.gtf >> WT8IsoSeq.NovelGenes.Antisense.bed
gtf2bed < $Mouse/WT8IsoSeq.collapsed.filtered.rep_corrected.gtf>> WT8IsoSeq.collapsed.bed 

gtf2bed < combined.NovelGenes.Antisense.gtf >> combined.NovelGenes.Antisense.bed


#************************************* 4. Use bedtools to find overlapping regions   
## Bedtools parameters
#-split = report only exons overlap 
#-S = overlaps be found on opposite strands 
#-wo = amount of overlap 
#-wa = report the original "A" feature, -wb = report the original "B" feature 
#-loj = Perform a “left outer join”. That is, for each feature in A report each overlap with B. If no overlaps are found, report a NULL feature for B.
#-header = Print the header from the A file prior to results.
#-C Reporting the number of overlapping features for each database file'

# Report depends on the bed file assigned first (-a), therefore switch between bed file containing novel, antsense genes and all other genes 
# human 
bedtools intersect -a combined.NovelGenes.Antisense.bed -b combined.No_Antisense.bed -split -S -wo > combined.NovelGenes.Antisense.output 
bedtools intersect -a combined.No_Antisense.bed -b combined.NovelGenes.Antisense.bed -split -S -wo > combined.NovelGenes.Antisense.output_collapsed

# mouse 
bedtools intersect -a WT8IsoSeq.NovelGenes.Antisense.bed -b WT8IsoSeq.No_Antisense.bed -split -S -wo > WT8.NovelGenes.Antisense.output 
bedtools intersect -a WT8IsoSeq.No_Antisense.bed -b WT8IsoSeq.NovelGenes.Antisense.bed -split -S -wo > WT8.NovelGenes.Antisense.output_collapsed

# Antisense, novel genes may have overlap to exonic regions to known genes that are not detected in Iso-Seq dataset
# therefore background with genome.gtf  
# bedtools gtf does not work directly with genome.gtf (https://www.biostars.org/p/56280/)
cat $REFERENCE/gencode.v31.annotation.gtf | grep transcript_id | grep gene_id | convert2bed --do-not-sort --input=gtf - > gencode.v31.annotation.bed
cat $REFERENCE/gencode.vM22.annotation.gtf | grep transcript_id | grep gene_id | convert2bed --do-not-sort --input=gtf - > gencode.M22.annotation.bed

bedtools intersect -a combined.NovelGenes.Antisense.bed -b gencode.v31.annotation.bed -split -S -wo > combined.NovelGenes.Antisense.genome.output 
bedtools intersect -a WT8IsoSeq.NovelGenes.Antisense.bed -b gencode.M22.annotation.bed -split -S -wo > WT8.NovelGenes.Antisense.genome.output 
